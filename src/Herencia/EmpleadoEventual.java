/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author patri
 */
public class EmpleadoEventual extends Empleado{
    private float pagoHoras;
    private float hrsTrabajadas;

    public EmpleadoEventual(float pagoHoras, float hrsTrabajadas, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoHoras = pagoHoras;
        this.hrsTrabajadas = hrsTrabajadas;
    }
    
    public EmpleadoEventual(){
    super();
    this.hrsTrabajadas = 0.0f;
    this.pagoHoras = 0.0f;
    
    }

    public float getPagoHoras() {
        return pagoHoras;
    }

    public void setPagoHoras(float pagoHoras) {
        this.pagoHoras = pagoHoras;
    }

    public float getHrsTrabajadas() {
        return hrsTrabajadas;
    }

    public void setHrsTrabajadas(float hrsTrabajadas) {
        this.hrsTrabajadas = hrsTrabajadas;
    }
    
    
    

    @Override
    public float calcularPago() {
    float pago = 0.0f;
    pago = this.hrsTrabajadas * this.pagoHoras;
    return pago;
    }
    
}
