/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author patri
 */
public  abstract class  Empleado {
   protected int numEmpleado;
   protected String nombre;
   protected String puesto;
   protected String depto;
   
   // constructores

    public Empleado(int numEmpleado, String nombre, String puesto, String depto) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.puesto = puesto;
        this.depto = depto;
    }

    public Empleado() {
        this.numEmpleado = 0;
        this.nombre = "";
        this.puesto = "";
        this.depto = "";
        
    }
    
    // set and get

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDepto() {
        return depto;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }
    
    public abstract float calcularPago();
    
   
    
   
    
}
