/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author patri
 */
public class EmpleadoBase extends Empleado implements impuestos {
    private float pagoDiario;
    private float diasTrabajados;
    
    // constructores

    public EmpleadoBase(float pagoDiario, float diasTrabajados, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoDiario = pagoDiario;
        this.diasTrabajados = diasTrabajados;
    }
    
    
    public EmpleadoBase(){
        super();
        this.diasTrabajados = 0.0f;
        this.pagoDiario = 0.0f;
    
    }

    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public float getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(float diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    
        @Override
    public float calcularPago() {
        return this.diasTrabajados * this.pagoDiario;
    }

    @Override
    public float calcularImpuesto() {
        float pago = 0;
        if (this.calcularPago()>= 15000) pago = this.calcularImpuesto()*.15f;
        return pago;
        
    }
    
}
